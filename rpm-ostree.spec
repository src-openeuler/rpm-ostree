Name:       rpm-ostree
Version:    2024.4
Release:    3
Summary:    Hybrid image/package system
License:    LGPL-2.0-or-later
URL:        https://github.com/coreos/rpm-ostree
Source0:    https://github.com/coreos/rpm-ostree/releases/download/v%{version}/%{name}-%{version}.tar.xz
#Fix missing patch in rpm-ostree.src.rpm
Source1:    0001-add-loongarch64-support-not-upstream-modified-files.patch
Source2:    0002-add-sw_64-support-not-upstream-modified-files.patch

Patch0:     0001-Revert-compose-Inject-our-static-tmpfiles.d-dropins-.patch
Patch1:     0001-passwd-create-etc-g-shadow-with-mode-0.patch
Patch2:     0002-unit-chmod-etc-g-shadow-to-0000.patch
Patch3:     0001-fix-compose-err.patch
Patch4:     0003-add-for-support-NestOS-kernel.patch
Patch5:     rpm-ostree-2024.4-libdnf-port-to-newer-cmake.patch

%if !%{defined rust_arches}
%define rust_arches x86_64 i686 armv7hl aarch64 ppc64 ppc64le s390x riscv64 loongarch64 sw_64
%endif

ExclusiveArch: %{rust_arches}

%ifarch loongarch64
Patch5:  0001-add-loongarch64-support-not-upstream-modified-files.patch
%endif

%ifarch sw_64
Patch6:  0002-add-sw_64-support-not-upstream-modified-files.patch
%endif

BuildRequires: make
BuildRequires: rust-packaging
BuildRequires: cargo
BuildRequires: rust

# Enable ASAN + UBSAN
%bcond_with sanitizers
# Embedded unit tests
%bcond_with bin_unit_tests

%bcond_with zchunk

# For the autofiles bits below
BuildRequires: python3-devel
# We always run autogen.sh
BuildRequires: autoconf automake libtool git
# For docs
BuildRequires: chrpath
BuildRequires: gtk-doc
BuildRequires: /usr/bin/g-ir-scanner
# Core requirements
# One way to check this: `objdump -p /path/to/rpm-ostree | grep LIBOSTREE` and pick the highest (though that might miss e.g. new struct members)
BuildRequires: pkgconfig(ostree-1) >= 2023.7
BuildRequires: pkgconfig(polkit-gobject-1)
BuildRequires: pkgconfig(json-glib-1.0)
BuildRequires: pkgconfig(rpm) >= 4.16.0
BuildRequires: pkgconfig(libarchive)
BuildRequires: pkgconfig(libsystemd)
BuildRequires: libcap-devel
BuildRequires: libattr-devel

# We currently interact directly with librepo (libdnf below also pulls it in,
# but duplicating to be clear)
BuildRequires: pkgconfig(librepo)

# Needed by curl-rust
BuildRequires: pkgconfig(libcurl)

BuildRequires: cmake
BuildRequires: pkgconfig(expat)
BuildRequires: pkgconfig(check)

# We use some libsolv types directly too (libdnf below also pulls it in,
# but duplicating to be clear)
BuildRequires: pkgconfig(libsolv)

# These are build deps which aren't strictly required in Koji/Brew builds, but
# are required for git builds. Since they're few and tiny, we just add it here
# to keep it part of `dnf builddep`.
BuildRequires: jq

#########################################################################
#                         libdnf build deps                             #
#                                                                       #
# Copy/pasted from libdnf/libdnf.spec. Removed the irrelevant bits like #
# valgrind, rhsm, swig, python, and sanitizer stuff.                    #
#########################################################################

%global libsolv_version 0.7.21
%global libmodulemd_version 2.13.0
%global librepo_version 1.13.1

BuildRequires:  cmake
BuildRequires:  gcc
BuildRequires:  gcc-c++
BuildRequires:  libsolv-devel >= %{libsolv_version}
BuildRequires:  pkgconfig(librepo) >= %{librepo_version}
BuildRequires:  pkgconfig(check)
BuildRequires:  pkgconfig(gio-unix-2.0) >= 2.50.0
BuildRequires:  pkgconfig(gtk-doc)
BuildRequires:  rpm-devel >= 4.16.0
%if %{with zchunk}
BuildRequires:  pkgconfig(zck) >= 0.9.11
%endif
BuildRequires:  pkgconfig(sqlite3)
BuildRequires:  pkgconfig(json-c)
BuildRequires:  pkgconfig(cppunit)
BuildRequires:  pkgconfig(modulemd-2.0) >= %{libmodulemd_version}
BuildRequires:  pkgconfig(smartcols)
BuildRequires:  gettext
BuildRequires:  gpgme-devel

Requires:       libmodulemd%{?_isa} >= %{libmodulemd_version}
Requires:       libsolv%{?_isa} >= %{libsolv_version}
Requires:       librepo%{?_isa} >= %{librepo_version}

#########################################################################
#                     end of libdnf build deps                          #
#########################################################################

# For now...see https://github.com/projectatomic/rpm-ostree/pull/637
# and https://github.com/fedora-infra/fedmsg-atomic-composer/pull/17
# etc.  We'll drop this dependency at some point in the future when
# rpm-ostree wraps more of ostree (such as `ostree admin unlock` etc.)
Requires: ostree
Requires: bubblewrap
Requires: fuse

# For container functionality
# https://github.com/coreos/rpm-ostree/issues/3286
Requires: skopeo

Requires: %{name}-libs%{?_isa} = %{version}-%{release}

%description
rpm-ostree is a hybrid image/package system.  It supports
"composing" packages on a build server into an OSTree repository,
which can then be replicated by client systems with atomic upgrades.
Additionally, unlike many "pure" image systems, with rpm-ostree
each client system can layer on additional packages, providing
a "best of both worlds" approach.

%package libs
Summary: Shared library for %{name}

%description libs
The %{name}-libs package includes the shared library for %{name}.

%package devel
Summary: Development headers for %{name}
Requires: %{name}-libs%{?_isa} = %{version}-%{release}

%description devel
The %{name}-devel package includes the header files for %{name}-libs.

%prep
%autosetup -Sgit -n %{name}-%{version} -p1
%if 0%{?__isa_bits} == 32
sed -ie 's,^lto = true,lto = false,' Cargo.toml
%endif

%build
env NOCONFIGURE=1 ./autogen.sh
# Since we're hybrid C++/Rust we need to propagate this manually;
# the %%configure macro today assumes (reasonably) that one is building
# C/C++ and sets C{,XX}FLAGS
%if 0%{?build_rustflags:1}
export RUSTFLAGS="%{build_rustflags}"
%endif
%configure --disable-silent-rules --enable-gtk-doc %{?rpmdb_default} %{?with_sanitizers:--enable-sanitizers} %{?with_bin_unit_tests:--enable-bin-unit-tests}

%make_build

%install
%make_install INSTALL="install -p -c"
find $RPM_BUILD_ROOT -name '*.la' -delete

cat > autofiles.py <<EOF
import os,sys,glob
os.chdir(os.environ['RPM_BUILD_ROOT'])
for line in sys.argv[1:]:
    if line == '':
        break
    if line[0] != '/':
        sys.stdout.write(line + '\n')
    else:
        files = glob.glob(line[1:])
        if len(files) > 0:
            sys.stderr.write('{0} matched {1} files\n'.format(line, len(files)))
            sys.stdout.write(line + '\n')
        else:
            sys.stderr.write('{0} did not match any files\n'.format(line))
EOF
PYTHON=python3
if ! test -x /usr/bin/python3; then
    PYTHON=python2
fi
$PYTHON autofiles.py > files \
  '%{_bindir}/*' \
  '%{_libdir}/%{name}' \
  '%{_mandir}/man*/*' \
  '%{_datadir}/dbus-1/system.d/*' \
  '%{_sysconfdir}/rpm-ostreed.conf' \
  '%{_prefix}/lib/systemd/system/*' \
  '%{_libexecdir}/rpm-ostree*' \
  '%{_libexecdir}/libostree/ext/*' \
  '%{_datadir}/polkit-1/actions/*.policy' \
  '%{_datadir}/dbus-1/system-services/*' \
  '%{_datadir}/bash-completion/completions/*'

$PYTHON autofiles.py > files.lib \
  '%{_libdir}/*.so.*' \
  '%{_libdir}/girepository-1.0/*.typelib'

$PYTHON autofiles.py > files.devel \
  '%{_libdir}/lib*.so' \
  '%{_includedir}/*' \
  '%{_datadir}/dbus-1/interfaces/org.projectatomic.rpmostree1.xml' \
  '%{_libdir}/pkgconfig/*' \
  '%{_datadir}/gtk-doc/html/*' \
  '%{_datadir}/gir-1.0/*-1.0.gir'

# Setup rpm-ostree-countme.timer according to presets
%post
%systemd_post rpm-ostree-countme.timer
# Only enable on rpm-ostree based systems and manually force unit enablement to
# explicitly ignore presets for this security fix
if [ -e /run/ostree-booted ]; then
    ln -snf /usr/lib/systemd/system/rpm-ostree-fix-shadow-mode.service  /usr/lib/systemd/system/multi-user.target.wants/
fi

%preun
%systemd_preun rpm-ostree-countme.timer

%postun
%systemd_postun_with_restart rpm-ostree-countme.timer

%files -f files
%doc COPYING.GPL COPYING.LGPL LICENSE README.md

%files libs -f files.lib

%files devel -f files.devel

%changelog
* Fri Mar 14 2025 Funda Wang <fundawang@yeah.net> - 2024.4-3
- fix build with cmake 4.0

* Mon May 27 2024 yueyuankun <yueyuankun@kylinos.cn> - 2024.4-2
- Fix missing patch in source code package

* Mon Apr 22 2024 lijian <lijian2@kylinos.cn> - 2024.4-1
- Update to 2024.4

* Wed Apr 10 2024 chendexi <chendexi@kylinos.cn> - 2023.7-2
- Synchronize upstream spec file modifications
- Fix CVE-2024-2905

* Wed Feb 21 2024 chendexi <chendexi@kylinos.cn> - 2023.7-1
- update to 2023.7

* Fri Oct 13 2023 wangyueliang <wangyueliang@kylinos.cn> - 2022.16-4
- merge feature from branch openEuler-22.03-LTS-SP1：
- DESC: add loongarch64 and sw_64 support
- AUTHOR: panchenbo <panchenbo@kylinsec.com.cn>

* Mon Sep 18 2023 wangyueliang <wangyueliang@kylinos.cn> - 2022.16-3
- fix compose error for selinux.
- add-for-support-NestOS-kernel.

* Fri Aug 04 2023 Jingwiw  <wangjingwei@iscas.ac.cn> - 2022.16-2
- enable riscv64 architecture

* Thu Feb 23 2023 fushanqing <fushanqing@kylinos.cn> - 2022.16-1
- Update rpm-ostree to 2022.16.

* Thu Dec 29 2022 chenmaodong <chenmaodong@xfusion.com> - 2022.8-4
- Fix has_modules_enable() check

* Mon Oct 10 2022 fushanqing <fushanqing@kylinos.cn> - 2022.8-3
- add fix-compose-err.patch

* Wed Aug 03 2022 liukuo <liukuo@kylinos.cn> - 2022.8-2
- License compliance rectification

* Tue Jun 07 2022 fushanqing <fushanqing@kylinos.cn> - 2022.8-1
- Update rpm-ostree to 2022.8

* Wed May 11 2022 duyiwei <duyiwei@kylinos.cn> - 2022.7-2
- update URL (projectatomic->coreos)

* Thu Apr 14 2022 yaoxin <yaoxin30@h-partners.com> - 2022.7-1
- Update rpm-ostree to 2022.7

* Fri Sep 10 2021 gaihuiying <gaihuiying1@huawei.com> - 2018.8-4
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:remove rpath of rpm-ostree's binary files

* Mon May 31 2021 huanghaitao <huanghaitao8@huawei.com> - 2018.8-3
- Completing build dependencies to fix gcc/gcc-c++ compiler missing error

* Sat Oct 19 2019 openEuler Buildteam <buildteam@openeuler.org> - 2018.8-2
- Package init
